import json
from operator import attrgetter
import os.path
from Models import Notification, NotificationEncoder


class Logic:
    def __init__(self):
        self.current_notification = None
        self.notifications_list = list()

    def add_notification(self, notification: Notification):
        self.notifications_list.append(notification)
        self.sort_notifications()

    def initialize_data(self) -> None:
        if os.path.isfile('notifications.txt'):
            with open('notifications.txt') as fp:
                file = fp.read()
                if len(file) > 0:
                    notifications_string = json.loads(file)
                    for notification in notifications_string:
                        temp = Notification("", "", 0, False, 0)
                        temp.decode(notification)
                        self.notifications_list.append(temp)

    def sort_notifications(self):
        self.notifications_list = sorted(self.notifications_list, key=attrgetter("alert_time"), reverse=False)

    def save_data(self) -> None:
        if len(self.notifications_list) > 0:
            with open('notifications.txt', 'w') as fp:
                json_data = json.dumps(self.notifications_list, cls=NotificationEncoder)
                fp.write(json_data)
        else:
            with open('notifications.txt', 'w') as fp:
                fp.write("")

    def delete_notification(self, notification_index: int):
        self.notifications_list.pop(notification_index)



