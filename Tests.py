import unittest
import Controller
from Models import Notification


class LogicTest(unittest.TestCase):
    def test_add_notification_to_list(self):
        logic = Controller.Logic()
        notification = Notification("", "", 0, 0, 0)
        logic.add_notification(notification)
        check_list = list()
        check_list.append(notification)
        self.assertListEqual(logic.notifications_list, check_list)

    def test_notification_list_order(self):
        pass
