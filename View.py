#!/usr/bin/python3
import sys
import time
import traceback

from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QWidget,
                             QHBoxLayout, QVBoxLayout,
                             QGroupBox, QLineEdit,
                             QDateTimeEdit, QComboBox, QTableWidget, QLabel, QPushButton, QSpinBox, QMessageBox,
                             QSystemTrayIcon, QAction, QMenu, QStyle, QTableWidgetItem)
from playsound import playsound

from Controller import Logic
from Models import Notification


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()
        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.is_already_playing = False
        self.play = True
        self.suspended = False
        self.logic = Logic()
        self.logic.initialize_data()
        self.list_of_action_buttons = list()
        self.tray_icon = QSystemTrayIcon(self)
        self.icon = QIcon('./resources/icons/icon.png')
        self.tray_icon.setIcon(self.icon)
        show = QAction("Show", self)
        hide = QAction("Hide", self)
        quit_app = QAction("Quit", self)
        show.triggered.connect(self.show)
        hide.triggered.connect(self.hide)
        quit_app.triggered.connect(self.quit)
        menu = QMenu()
        menu.addAction(show)
        menu.addAction(hide)
        menu.addAction(quit_app)
        self.tray_icon.setContextMenu(menu)
        self.tray_icon.show()
        self.pick_a_time_label = QLabel("Pick a time: ")
        self.add_notification_third_line = QHBoxLayout()
        self.cyclical_notification_details_types = ["Every day from", "Every certain time"]
        self.one_time_notification_details_types = ["At the exact time", "After a given time"]
        self.notifications_table = QTableWidget()
        self.notification_title = QLineEdit()
        self.notification_description = QLineEdit()
        self.notification_type = QComboBox()
        self.notification_details = QComboBox()
        self.notification_year = None
        self.notification_month = None
        self.notification_days = None
        self.notification_hours = None
        self.notification_minutes = None
        self.notification_seconds = None
        self.years_label = None
        self.month_label = None
        self.days_label = None
        self.hours_label = None
        self.minutes_label = None
        self.seconds_label = None
        self.notification_date_time = QDateTimeEdit()
        self.add_notification_button = QPushButton()
        self.init_ui()
        self.threadpool = QThreadPool()
        self.timer_start()

    def quit(self) -> None:
        reply = QMessageBox.question(self, "Confirmation", "Are you sure you want to quit?",
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.tray_icon.hide()
            self.logic.save_data()
            self.threadpool.clear()
            self.suspended = True;
            QApplication.exit()

    def closeEvent(self, event) -> None:
        self.hide()
        self.tray_icon.showMessage("Reminder", "Application was minimized to tray", QSystemTrayIcon.Information, 2000)
        event.ignore()

    def init_ui(self):
        super(MainWindow, self).setWindowIcon(QIcon("./resources/icons/icon.png"))
        vertical_box = QVBoxLayout()
        self.setLayout(vertical_box)
        notifications_group = QGroupBox()
        notifications_group.setCheckable(False)
        notifications_group.setTitle("Upcoming notifications.")
        vertical_box.addWidget(notifications_group)
        vertical_table_box = QVBoxLayout()
        notifications_group.setLayout(vertical_table_box)
        vertical_table_box.addWidget(self.notifications_table)
        self.notifications_table.setColumnCount(5)
        header = self.notifications_table.horizontalHeader()

        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.ResizeToContents)
        self.notifications_table.setRowCount(0)
        self.notifications_table.setHorizontalHeaderLabels(['Title', 'Description', 'Notification type', 'Time',
                                                             'Action'])

        add_notification_group = QGroupBox()
        add_notification_group.setTitle("Add new notification.")
        add_notification_group.setCheckable(False)
        vertical_box.addWidget(add_notification_group)
        add_notification_group_layout = QVBoxLayout()
        add_notification_group.setLayout(add_notification_group_layout)
        add_notification_first_line = QHBoxLayout()
        add_notification_second_line = QHBoxLayout()
        add_notification_fourth_line = QHBoxLayout()
        add_notification_group_layout.addLayout(add_notification_first_line)
        add_notification_group_layout.addLayout(add_notification_second_line)
        add_notification_group_layout.addLayout(self.add_notification_third_line)
        add_notification_group_layout.addLayout(add_notification_fourth_line)
        date_time = QDateTime.currentDateTime()
        time = QTime.currentTime()
        time = time.addSecs(-time.second())
        date_time.setTime(time)
        self.notification_date_time.setDateTime(date_time)
        self.notification_date_time.setMinimumDateTime(QDateTime.currentDateTime())
        self.notification_date_time.setMaximumWidth(150)
        self.notification_type.activated[int].connect(self.handle_notification_type_changed)
        notification_type_items = ["One-time notification", "Cyclical notification"]
        self.notification_type.addItems(notification_type_items)
        self.notification_details.addItems(self.one_time_notification_details_types)
        self.notification_details.activated[int].connect(self.handle_notification_details_changed)
        add_notification_first_line.addWidget(QLabel("Title:"))
        add_notification_first_line.addWidget(self.notification_title)
        add_notification_first_line.addStretch()
        add_notification_first_line.addWidget(QLabel("Notification type:"))
        add_notification_first_line.addWidget(self.notification_type)
        add_notification_second_line.addWidget(QLabel("Description:"))
        add_notification_second_line.addWidget(self.notification_description)
        add_notification_second_line.addStretch()
        add_notification_second_line.addWidget(QLabel("Notification details"))
        add_notification_second_line.addWidget(self.notification_details)
        self.add_notification_third_line.addStretch()
        self.add_notification_third_line.addWidget(self.pick_a_time_label)
        self.add_notification_third_line.addWidget(self.notification_date_time)
        self.add_notification_button.clicked.connect(self.handle_add_button)
        self.add_notification_button.setText("Add")
        add_notification_fourth_line.addStretch()
        add_notification_fourth_line.addWidget(self.add_notification_button)
        self.setGeometry(600, 300, 600, 300)
        self.setWindowTitle("Reminder")
        self.show()
        self.show_notifications()

    def show_notifications(self) -> None:
        self.notifications_table.setRowCount(len(self.logic.notifications_list))
        self.list_of_action_buttons = list()

        for i, v in enumerate(self.logic.notifications_list):
            self.notifications_table.setItem(i, 0, QTableWidgetItem(v.title))
            self.notifications_table.setItem(i, 1, QTableWidgetItem(v.description))
            push_button = QPushButton("Delete")
            push_button.clicked.connect(self.delete_notification)
            push_button.setStyleSheet("QPushButton{ background-color: #918e84 }")
            self.list_of_action_buttons.append(push_button)
            self.notifications_table.setCellWidget(i, 4, push_button)
            if v.is_cyclical:
                self.notifications_table.setItem(i, 2, QTableWidgetItem("Cyclical"))
            else:
                self.notifications_table.setItem(i, 2, QTableWidgetItem("One-time"))
            self.notifications_table.setItem(i, 3, QTableWidgetItem(
                QDateTime.fromSecsSinceEpoch(v.alert_time).toString("yyyy-MM-dd HH:mm:ss")))

    def delete_notification(self) -> None:
        self.notifications_table.removeRow(self.list_of_action_buttons.index(self.sender()))
        self.logic.notifications_list.pop(self.list_of_action_buttons.index(self.sender()))
        self.list_of_action_buttons.pop(self.list_of_action_buttons.index(self.sender()))
        self.logic.save_data()

    def handle_add_button(self) -> None:
        if self.notification_type.currentIndex() == 0:
            if self.notification_details.currentIndex() == 0:
                if QDateTime.currentDateTime().secsTo(self.notification_date_time.dateTime()) > 0:
                    self.logic.add_notification(
                        Notification(self.notification_title.text(), self.notification_description.text(),
                                     self.notification_date_time.dateTime().toSecsSinceEpoch(), False, 0))
                    self.logic.save_data()
                    self.show_notifications()

                else:
                    error_msg = QMessageBox()
                    error_msg.setIcon(QMessageBox.Critical)
                    error_msg.setText("Error")
                    error_msg.setInformativeText('Date must be greater than current date!')
                    error_msg.setWindowTitle("Error")
                    error_msg.exec_()
            else:
                new_date = QDateTime().currentDateTime()
                new_date = new_date.addYears(self.notification_year.value())
                new_date = new_date.addMonths(self.notification_month.value())
                new_date = new_date.addDays(self.notification_days.value())
                new_date = new_date.addSecs(self.notification_hours.value() * 60 * 60)
                new_date = new_date.addSecs(self.notification_minutes.value() * 60)
                new_date = new_date.addSecs(self.notification_seconds.value())
                self.logic.add_notification(
                    Notification(self.notification_title.text(), self.notification_description.text(),
                                 new_date.toSecsSinceEpoch(), False, 0))
                self.show_notifications()

        else:
            if self.notification_details.currentIndex() == 0:
                if QDateTime.currentDateTime().secsTo(self.notification_date_time.dateTime()) > 0:
                    self.logic.add_notification(
                        Notification(self.notification_title.text(), self.notification_description.text(),
                                     self.notification_date_time.dateTime().toSecsSinceEpoch(), True,
                                     1000 * 60 * 60 * 24))
                    self.show_notifications()

                else:
                    error_msg = QMessageBox()
                    error_msg.setIcon(QMessageBox.Critical)
                    error_msg.setText("Error")
                    error_msg.setInformativeText('Date must be greater than current date!')
                    error_msg.setWindowTitle("Error")
                    error_msg.exec_()
            else:
                new_date = QDateTime().currentDateTime()
                new_date = new_date.addYears(self.notification_year.value())
                new_date = new_date.addMonths(self.notification_month.value())
                new_date = new_date.addDays(self.notification_days.value())
                new_date = new_date.addSecs(self.notification_hours.value() * 60 * 60)
                new_date = new_date.addSecs(self.notification_minutes.value() * 60)
                new_date = new_date.addSecs(self.notification_seconds.value())
                date_difference = QDateTime.currentDateTime().secsTo(new_date)
                self.logic.add_notification(
                    Notification(self.notification_title.text(), self.notification_description.text(),
                                 new_date.toSecsSinceEpoch(), True, date_difference))
                self.show_notifications()

    def show_notification_message(self):
        error_msg = QMessageBox()
        error_msg.setIcon(QMessageBox.Information)
        error_msg.setText(self.logic.current_notification.title)
        error_msg.setInformativeText(self.logic.current_notification.description)
        error_msg.setWindowTitle("Notification")
        error_msg.exec()

        self.play = False

    def handle_notification_type_changed(self, index) -> None:
        if index == 0:
            current_index = self.notification_details.currentIndex()
            self.notification_details.clear()
            self.notification_details.addItems(self.one_time_notification_details_types)
            self.notification_details.setCurrentIndex(current_index)
        else:
            current_index = self.notification_details.currentIndex()
            self.notification_details.clear()
            self.notification_details.addItems(self.cyclical_notification_details_types)
            self.notification_details.setCurrentIndex(current_index)

    def handle_notification_details_changed(self, index) -> None:
        if index == 0:
            if self.notification_year is not None:
                self.add_notification_third_line.removeWidget(self.years_label)
                self.years_label.deleteLater()
                self.years_label = None
                self.add_notification_third_line.removeWidget(self.notification_year)
                self.notification_year.deleteLater()
                self.notification_year = None

                self.add_notification_third_line.removeWidget(self.month_label)
                self.month_label.deleteLater()
                self.month_label = None
                self.add_notification_third_line.removeWidget(self.notification_month)
                self.notification_month.deleteLater()
                self.notification_month = None

                self.add_notification_third_line.removeWidget(self.days_label)
                self.days_label.deleteLater()
                self.days_label = None
                self.add_notification_third_line.removeWidget(self.notification_days)
                self.notification_days.deleteLater()
                self.notification_days = None

                self.add_notification_third_line.removeWidget(self.hours_label)
                self.hours_label.deleteLater()
                self.hours_label = None
                self.add_notification_third_line.removeWidget(self.notification_hours)
                self.notification_hours.deleteLater()
                self.notification_hours = None

                self.add_notification_third_line.removeWidget(self.minutes_label)
                self.minutes_label.deleteLater()
                self.minutes_label = None
                self.add_notification_third_line.removeWidget(self.notification_minutes)
                self.notification_minutes.deleteLater()
                self.notification_minutes = None

                self.add_notification_third_line.removeWidget(self.seconds_label)
                self.seconds_label.deleteLater()
                self.seconds_label = None
                self.add_notification_third_line.removeWidget(self.notification_seconds)
                self.notification_seconds.deleteLater()
                self.notification_seconds = None

                self.pick_a_time_label = QLabel("Pick a time:")
                self.add_notification_third_line.addWidget(self.pick_a_time_label)
                self.notification_date_time = QDateTimeEdit()
                self.notification_date_time.setMaximumWidth(150)
                date_time = QDateTime.currentDateTime()
                time = QTime.currentTime()
                time = time.addSecs(-time.second())
                date_time.setTime(time)
                self.notification_date_time.setDateTime(date_time)
                self.notification_date_time.setMinimumDateTime(date_time)
                self.add_notification_third_line.addWidget(self.notification_date_time)

        else:
            if self.notification_date_time is not None:
                self.add_notification_third_line.removeWidget(self.notification_date_time)
                self.notification_date_time.deleteLater()
                self.notification_date_time = None
                self.add_notification_third_line.removeWidget(self.pick_a_time_label)
                self.pick_a_time_label.deleteLater()
                self.pick_a_time_label = None
                self.notification_year = QSpinBox()
                self.notification_month = QSpinBox()
                self.notification_days = QSpinBox()
                self.notification_hours = QSpinBox()
                self.notification_minutes = QSpinBox()
                self.notification_seconds = QSpinBox()
                self.notification_year.setMaximumWidth(40)
                self.notification_month.setMaximumWidth(40)
                self.notification_days.setMaximumWidth(40)
                self.notification_hours.setMaximumWidth(40)
                self.notification_minutes.setMaximumWidth(40)
                self.notification_seconds.setMaximumWidth(40)
                self.years_label = QLabel("Years")
                self.month_label = QLabel("Months:")
                self.days_label = QLabel("Days:")
                self.hours_label = QLabel("Hours:")
                self.minutes_label = QLabel("Minutes:")
                self.seconds_label = QLabel("Seconds:")
                self.add_notification_third_line.addWidget(self.years_label)
                self.add_notification_third_line.addWidget(self.notification_year)
                self.add_notification_third_line.addWidget(self.month_label)
                self.add_notification_third_line.addWidget(self.notification_month)
                self.add_notification_third_line.addWidget(self.days_label)
                self.add_notification_third_line.addWidget(self.notification_days)
                self.add_notification_third_line.addWidget(self.hours_label)
                self.add_notification_third_line.addWidget(self.notification_hours)
                self.add_notification_third_line.addWidget(self.minutes_label)
                self.add_notification_third_line.addWidget(self.notification_minutes)
                self.add_notification_third_line.addWidget(self.seconds_label)
                self.add_notification_third_line.addWidget(self.notification_seconds)

    def timer_start(self):
        worker = Worker(self.check_notification_time)
        worker.signals.progress.connect(self.show_notifications)
        self.threadpool.start(worker)

    def play_alert_thread(self):
        worker = Worker(self.play_alert)
        worker.signals.progress.connect(self.show_notification_message)
        self.threadpool.start(worker)

    def play_alert(self, progress_callback) -> None:
        progress_callback.emit(0)
        if not self.is_already_playing:
            self.is_already_playing = True
            while self.play:
                playsound("./resources/sounds/alert.mp3")
            self.play = True
            self.is_already_playing = False

    def check_notification_time(self, progress_callback):
        while not self.suspended:
            if len(self.logic.notifications_list) > 0:
                for notification in self.logic.notifications_list:
                    if notification.alert_time < time.time():
                        self.play_alert_thread()
                        self.logic.current_notification = notification
                        if notification.is_cyclical:
                            while notification.alert_time < time.time():
                                notification.alert_time += notification.period
                            self.logic.sort_notifications()
                            progress_callback.emit(0)
                        else:
                            self.logic.notifications_list.pop(0)
                            self.notifications_table.removeRow(0)
                            self.list_of_action_buttons.pop(0)
                            progress_callback.emit(0)
                        self.logic.save_data()
                        break
            time.sleep(1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    ex = MainWindow()
    sys.exit(app.exec_())
