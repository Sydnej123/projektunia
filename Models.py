import json
from json import JSONEncoder


class Notification:
    def __init__(self, title: str, description: str, alert_time: int, is_cyclical: bool, period: int):
        self.title = title
        self.description = description
        self.alert_time = alert_time
        self.is_cyclical = is_cyclical
        self.period = period

    def __repr__(self):
        return str(self.__dict__)

    def encode(self):
        return json.dumps(self.__dict__)

    def decode(self, data):
        self.__dict__.update((json.loads(data)))


class NotificationEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Notification):
            return o.encode()
        return NotificationEncoder(self, o)

